package net.tanozin.books.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.tanozin.books.data.entities.Volume
import net.tanozin.books.data.remote.ApiResponse
import net.tanozin.books.domain.GetBooksUseCase

class VolumeListViewModel constructor(private val getBooksUseCase: GetBooksUseCase): ViewModel(){
    private val _volumes = MutableLiveData<List<Volume>>()
    val volumes: LiveData<List<Volume>>
        get() = _volumes
    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading


    private var job: Job? = null

    init {
        getBookList("")
    }

    fun getBookList(query:String){
        _isLoading.value = true
        job = CoroutineScope(Dispatchers.Default).launch{
            val result = getBooksUseCase(query)
            _isLoading.postValue(false)
            when(result){
                is ApiResponse.Success -> _volumes.postValue(result.items)
                is ApiResponse.Error -> _error.postValue(result.errorMessage)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}