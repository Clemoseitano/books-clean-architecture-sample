package net.tanozin.books.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import net.tanozin.books.R
import net.tanozin.books.data.entities.Volume
import net.tanozin.books.di.Injector
import net.tanozin.books.utils.GlideApp

class VolumeDetailFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mViewModel =
            Injector.provideViewModelFactory().create(VolumeDetailViewModel::class.java)
        val rootView = inflater.inflate(R.layout.volume_detail_fragment, container, false)
        val volume = arguments?.let { VolumeDetailFragmentArgs.fromBundle(it).volume }
        val imageView: ImageView = rootView.findViewById(R.id.volume_cover_image_view)
        val titleView: TextView = rootView.findViewById(R.id.title_text_view)
        val authorView: TextView = rootView.findViewById(R.id.authors_text_view)
        val dateView: TextView = rootView.findViewById(R.id.pub_date_text_view)
        val descriptionView: TextView = rootView.findViewById(R.id.description_text_view)

        mViewModel.setVolume(volume)

        mViewModel.coverImage.observe(viewLifecycleOwner, { media ->
            GlideApp.with(imageView.context).load(media).placeholder(R.mipmap.ic_launcher)
                .into(imageView)
        })
        mViewModel.title.observe(viewLifecycleOwner, { title -> titleView.text = title })
        mViewModel.authors.observe(
            viewLifecycleOwner,
            { title -> authorView.text = title })
        mViewModel.pubDate.observe(viewLifecycleOwner, { title -> dateView.text = title })
        mViewModel.description.observe(
            viewLifecycleOwner,
            { title -> descriptionView.text = title })
        return rootView
    }
}