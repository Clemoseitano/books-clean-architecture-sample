package net.tanozin.books.ui

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import net.tanozin.books.R
import net.tanozin.books.data.entities.Volume
import net.tanozin.books.utils.GlideApp

class VolumeRecyclerViewAdapter(
    private val context: Context,
    private val values: List<Volume>,
    private val clickListener: ItemClickListener
) : RecyclerView.Adapter<VolumeRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.volume_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        val media = item.coverImage
        GlideApp.with(context).load(media).placeholder(R.mipmap.ic_launcher)
            .into(holder.coverImageView)
        holder.titleTextView.text = item.title
        holder.view.setOnClickListener { clickListener.onItemClicked(item) }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val coverImageView: ImageView = view.findViewById(R.id.volume_cover_image_view)
        val titleTextView: TextView = view.findViewById(R.id.volume_title_text_view)

        override fun toString(): String {
            return super.toString() + " '" + titleTextView.text + "'"
        }
    }

    interface ItemClickListener {
        fun onItemClicked(volume: Volume)
    }
}