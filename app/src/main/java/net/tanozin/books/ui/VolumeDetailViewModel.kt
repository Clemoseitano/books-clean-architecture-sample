package net.tanozin.books.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.tanozin.books.data.entities.Volume

class VolumeDetailViewModel : ViewModel() {
    private val _coverImage = MutableLiveData<String>()
    val coverImage: LiveData<String>
        get() = _coverImage
    private val _title = MutableLiveData<String>()
    val title: LiveData<String>
        get() = _title
    private val _authors = MutableLiveData<String>()
    val authors: LiveData<String>
        get() = _authors
    private val _pubDate = MutableLiveData<String>()
    val pubDate: LiveData<String>
        get() = _pubDate
    private val _description = MutableLiveData<String>()
    val description: LiveData<String>
        get() = _description

    fun setVolume(volume: Volume?) {
        if (volume != null) {
            _coverImage.postValue(volume.coverImage)
            _title.postValue(volume.title)
            _authors.postValue(volume.authors.replace("...", ", "))
            _pubDate.postValue(volume.publishedDate)
            _description.postValue(volume.description)
        }
    }
}