package net.tanozin.books.ui

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.tanozin.books.R
import net.tanozin.books.data.entities.Volume
import net.tanozin.books.di.Injector

/**
 * A fragment representing a list of Items.
 */
class VolumeListFragment : Fragment(), VolumeRecyclerViewAdapter.ItemClickListener {

    private val mVolumes: List<Volume> = ArrayList()
    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val mViewModel = Injector.provideViewModelFactory().create(VolumeListViewModel::class.java)
        rootView = inflater.inflate(R.layout.fragment_volume_list, container, false)
        val recyclerView = rootView.findViewById<RecyclerView>(R.id.volume_list_view)
        val searchInput = rootView.findViewById<EditText>(R.id.search_edit_text)
        val mVolumeAdapter = VolumeRecyclerViewAdapter(requireContext(), mVolumes, this)
        // Set the adapter
        if (recyclerView is RecyclerView) {
            with(recyclerView) {
                layoutManager = LinearLayoutManager(context)
                adapter = mVolumeAdapter
            }
        }

        searchInput.setImeOptions(EditorInfo.IME_ACTION_DONE)
        searchInput
            .setImeActionLabel(resources.getString(R.string.search), KeyEvent.KEYCODE_ENTER)
        searchInput.setOnEditorActionListener { _, actionId, _ ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val imm =
                    activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                imm?.hideSoftInputFromWindow(searchInput.windowToken, 0)
                val searchQuery = searchInput.text.toString()
                mViewModel.getBookList(searchQuery)
                handled = true
            }
            handled
        }

        mViewModel.volumes.observe(viewLifecycleOwner, Observer { results ->
            run {
                if (mVolumes is ArrayList) {
                    mVolumes.clear()
                    mVolumes.addAll(results)
                }
                mVolumeAdapter.notifyDataSetChanged()
            }
        })
        return rootView
    }

    override fun onItemClicked(volume: Volume) {
        val action = VolumeListFragmentDirections.actionNavVolumeListToNavVolumeDetail(volume)
        Navigation.findNavController(rootView).navigate(action)
    }
}