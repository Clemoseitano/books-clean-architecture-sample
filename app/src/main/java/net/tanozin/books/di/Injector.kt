package net.tanozin.books.di

import net.tanozin.books.data.cache.AppDatabase
import net.tanozin.books.data.cache.VolumeDao
import net.tanozin.books.data.remote.*
import net.tanozin.books.data.repository.BookRepository
import net.tanozin.books.data.repository.BookRepositoryImpl
import net.tanozin.books.domain.GetBooksUseCase
import okhttp3.OkHttpClient

class Injector {
    companion object {
        fun provideViewModelFactory(): ViewModelFactory {
            return ViewModelFactory(provideBooksUseCase())
        }

        private fun provideBooksUseCase(): GetBooksUseCase {
            return GetBooksUseCase(provideBooksRepository())
        }

        private fun provideBooksRepository(): BookRepository {
            return BookRepositoryImpl(
                provideBookAPIHelper(),
                provideBookDao(),
                APIInterface.API_KEY
            )
        }

        private fun provideBookDao(): VolumeDao {
            return provideDatabase().volumeDao()
        }

        private fun provideDatabase(): AppDatabase {
            return AppDatabase.provideRoomDB(BooksApp.applicationInstance)
        }

        private fun provideBookAPIHelper(): BookAPIHelper {
            return BookAPIHelperImpl(provideRetrofitImpl())
        }

        private fun provideRetrofitImpl(): APIInterface {
            return RetrofitBuilder(APIInterface.BASE_URL, provideOkHttpClient()).getBuilder()
                .create(APIInterface::class.java)
        }

        private fun provideOkHttpClient(): OkHttpClient {
            return OkHttpBuilder().createClient()
        }
    }
}