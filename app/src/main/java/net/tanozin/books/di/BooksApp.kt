package net.tanozin.books.di

import android.app.Application

class BooksApp : Application() {
    companion object {
        lateinit var applicationInstance: Application
    }

    override fun onCreate() {
        super.onCreate()
        applicationInstance = this
    }
}