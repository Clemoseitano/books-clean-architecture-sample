package net.tanozin.books.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.tanozin.books.domain.GetBooksUseCase
import net.tanozin.books.ui.VolumeDetailViewModel
import net.tanozin.books.ui.VolumeListViewModel

class ViewModelFactory(
    private val getBooksUseCase: GetBooksUseCase
) : ViewModelProvider.Factory {

    /**
     * Creates a new instance of the given `Class`.
     *
     *
     *
     * @param modelClass a `Class` whose instance is requested
     * @return a newly created ViewModel
     */
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            VolumeDetailViewModel::class.java -> VolumeDetailViewModel() as T
            else -> VolumeListViewModel(getBooksUseCase) as T
        }
    }
}