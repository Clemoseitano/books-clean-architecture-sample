package net.tanozin.books.data.remote

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class OkHttpBuilder {
    fun createClient(): OkHttpClient {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
        return httpClient.build()
    }
}