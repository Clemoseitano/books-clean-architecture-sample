package net.tanozin.books.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

/**
 * A class representing a volume of a book.
 */
@Entity(
    tableName = "book_volumes"
)
data class Volume(
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "cover_image")
    val coverImage: String,
    @ColumnInfo(name = "description")
    val description: String,
    @ColumnInfo(name = "authors")
    val authors: String,
    @ColumnInfo(name = "published_date")
    val publishedDate: String
) : Serializable {
    @PrimaryKey(
        autoGenerate = true
    )
    var id: Int? = null
}