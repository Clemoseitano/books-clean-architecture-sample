package net.tanozin.books.data.repository

import net.tanozin.books.data.entities.Volume
import net.tanozin.books.data.remote.ApiResponse

interface BookRepository {
    suspend fun getVolumes(query: String): ApiResponse<List<Volume>>
}