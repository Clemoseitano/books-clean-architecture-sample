package net.tanozin.books.data.remote

import kotlinx.coroutines.Deferred
import net.tanozin.books.data.entities.BooksResponse
import net.tanozin.books.data.entities.Volume
import okhttp3.ResponseBody
import retrofit2.Response

interface BookAPIHelper {
    fun getVolumesAsync(params: Map<String, String>): Deferred<Response<BooksResponse>>
}