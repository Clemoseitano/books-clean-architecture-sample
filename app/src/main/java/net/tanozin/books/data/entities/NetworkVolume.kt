package net.tanozin.books.data.entities

import com.google.gson.annotations.SerializedName
import net.tanozin.books.data.remote.DataMapper

data class NetworkVolume(
    @SerializedName("title")
    val title: String,
    @SerializedName("imageLinks")
    val coverImage: ImageInfo,
    @SerializedName("description")
    val description: String,
    @SerializedName("publishedDate")
    val publishedDate: String,
    @SerializedName("authors")
    val authors: List<String>
) : DataMapper<Volume> {
    fun authorsAsString(): String {
        var authorConcat = ""
        for (item in authors)
            authorConcat += if ("" == authorConcat) item else "...$item"
        return authorConcat
    }

    override fun mapData(): Volume {
        return Volume(
            title = title,
            coverImage = coverImage.thumbnail,
            description = description,
            publishedDate = publishedDate,
            authors = authorsAsString()
        )
    }
}

data class BooksResponse(
    @SerializedName("items")
    val items: List<ItemData>
)

data class ItemData(
    val volumeInfo: NetworkVolume
)