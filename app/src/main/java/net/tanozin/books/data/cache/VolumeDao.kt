package net.tanozin.books.data.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import net.tanozin.books.data.entities.Volume

@Dao
interface VolumeDao {
    @Query("SELECT * FROM book_volumes")
    fun getVolumes(): List<Volume>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveVolumes(volumes: List<Volume>)

    @Query("DELETE FROM book_volumes")
    fun deleteAllVolumes()
}