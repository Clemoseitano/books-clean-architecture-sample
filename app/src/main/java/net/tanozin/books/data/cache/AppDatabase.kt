package net.tanozin.books.data.cache

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import net.tanozin.books.data.entities.Volume

@Database(entities = [Volume::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun volumeDao(): VolumeDao

    companion object {
        fun provideRoomDB(context: Context): AppDatabase {
            return Room.databaseBuilder(
                context,
                AppDatabase::class.java, "local_cache.db"
            ).build()
        }
    }
}