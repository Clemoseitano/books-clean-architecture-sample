package net.tanozin.books.data.entities

import com.google.gson.annotations.SerializedName

data class ImageInfo(
    @SerializedName("thumbnail", alternate = ["smallThumbnail"])
    val thumbnail: String
)