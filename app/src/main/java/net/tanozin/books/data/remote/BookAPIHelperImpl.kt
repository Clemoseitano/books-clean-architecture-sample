package net.tanozin.books.data.remote

import kotlinx.coroutines.Deferred
import net.tanozin.books.data.entities.BooksResponse
import retrofit2.Response


class BookAPIHelperImpl(private val apiInterface: APIInterface) : BookAPIHelper {
    override fun getVolumesAsync(params: Map<String, String>): Deferred<Response<BooksResponse>> {
        return apiInterface.getVolumes(params)
    }
}