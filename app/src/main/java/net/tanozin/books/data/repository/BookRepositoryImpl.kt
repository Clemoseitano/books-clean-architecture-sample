package net.tanozin.books.data.repository

import androidx.annotation.VisibleForTesting
import net.tanozin.books.data.cache.VolumeDao
import net.tanozin.books.data.entities.BooksResponse
import net.tanozin.books.data.entities.Volume
import net.tanozin.books.data.remote.ApiResponse
import net.tanozin.books.data.remote.BookAPIHelper
import net.tanozin.books.safeApiCall

class BookRepositoryImpl constructor(
    private val bookAPIHelper: BookAPIHelper,
    private val volumeDao: VolumeDao,
    private val apiKey: String
) : BookRepository {
    private lateinit var lastQuery: String
    override suspend fun getVolumes(query: String): ApiResponse<List<Volume>> {
        val refresh = "" != query //&& query != lastQuery
        lastQuery = query
        return if (refresh) {
            safeApiCall(
                call = { getBookList() },
                errorMessage = "Exception occurred!"
            )
        } else {
            return ApiResponse.Success(volumeDao.getVolumes())
        }
    }

    private suspend fun getBookList(): ApiResponse<List<Volume>> {
        val params: MutableMap<String, String> = HashMap()
        params["q"] = "$lastQuery+intitle:$lastQuery"
        params["key"] = apiKey

        val result = bookAPIHelper.getVolumesAsync(params).await()

        if (result.isSuccessful) {
            val data: BooksResponse? = result.body()
            val items = data!!.items

            val volumeList = ArrayList<Volume>()
            for (item in items) {
                volumeList.add(item.volumeInfo.mapData())
            }
            cacheBooks(volumeList)
            return ApiResponse.Success(volumeList)
        }
        return ApiResponse.Error(result.code(), result.message())
    }

    @VisibleForTesting
    internal fun cacheBooks(books: List<Volume>?) {
        if (books != null) {
            volumeDao.deleteAllVolumes()
            volumeDao.saveVolumes(books)
        }
    }
}