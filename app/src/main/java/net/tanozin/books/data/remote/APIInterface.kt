package net.tanozin.books.data.remote

import kotlinx.coroutines.Deferred
import net.tanozin.books.data.entities.BooksResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface APIInterface {
    companion object {
        val BASE_URL: String
            get() = "https://www.googleapis.com/books/v1/"
        val API_KEY: String get() = "AIzaSyAAFSSTYy6yA5bn8Pbnda9t1A9D1HeX0YA"
    }

    @GET("volumes")
    fun getVolumes(@QueryMap params: Map<String, String>): Deferred<Response<BooksResponse>>
}