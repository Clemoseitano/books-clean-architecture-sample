package net.tanozin.books.data.remote

interface DataMapper<T> {
    fun mapData(): T
}