package net.tanozin.books.data.remote

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitBuilder(var baseUrl: String, var okHttpClient: OkHttpClient) {
    fun getBuilder(): Retrofit {
        return Retrofit.Builder().baseUrl(this.baseUrl).client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory()).build()
    }
}