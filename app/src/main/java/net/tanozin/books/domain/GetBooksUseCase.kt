package net.tanozin.books.domain

import net.tanozin.books.data.entities.Volume
import net.tanozin.books.data.remote.ApiResponse
import net.tanozin.books.data.repository.BookRepository

class GetBooksUseCase constructor(private val bookRepository: BookRepository) {

    suspend operator fun invoke(query: String): ApiResponse<List<Volume>> {
        return bookRepository.getVolumes(query)
    }
}