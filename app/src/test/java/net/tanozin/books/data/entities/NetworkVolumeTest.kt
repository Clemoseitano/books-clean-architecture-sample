package net.tanozin.books.data.entities

import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class NetworkVolumeTest {

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun `authorsAsString multiple authors concatenates names`() {
        // Given
        val authors = arrayListOf("Clement", "Osei", "Tano")
        val networkVolume: NetworkVolume = NetworkVolume(
            "Homecoming",
            ImageInfo("my_image"),
            "A short story",
            "2021-03-20",
            authors
        )
        // When
        val result: String = networkVolume.authorsAsString()
        // Assertion
        assert(result == "Clement...Osei...Tano")
    }

    @Test
    fun mapData() {
    }
}